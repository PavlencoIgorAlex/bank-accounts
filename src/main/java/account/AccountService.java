package account;

public interface AccountService {
    public Account getAccount(String id);
    public boolean AccountExist (String id);
}
