package account;

public class Account {

    private String id;
    private String number;
    private long balance;
    private String currency;
    private String timeOfTheLastOperation;

    public Account(String id, String number, long balance, String currency, String time_of_the_last_operation) {
        this.id = id;
        this.number = number;
        this.balance = balance;
        this.currency = currency;
        this.timeOfTheLastOperation = time_of_the_last_operation;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getTime_of_the_last_operation() {
        return timeOfTheLastOperation;
    }

    public void setTime_of_the_last_operation(String time_of_the_last_operation) {
        this.timeOfTheLastOperation  = time_of_the_last_operation;
    }
}
