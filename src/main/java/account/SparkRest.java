package account;

import com.google.gson.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import static spark.Spark.*;

public class SparkRest {

    static final AccountService accountService = new AccountServiceMapImpl();

    public static void main(String[] args) {
    //example GET http://localhost:4567/accounts/1
        get("/accounts/:id", (request, response) -> {
            response.type("application/json");
            return accountService.AccountExist(request.params(":id")) ?
                    new Gson().toJson(
                            new StandardResponse(StatusResponse.SUCCESS,new Gson()
                                    .toJsonTree(accountService.getAccount(request.params(":id"))))) :
                    new Gson().toJson(new StandardResponse(StatusResponse.ERROR, "Account not found")) ;
        });
    }
}
