package account;

import java.sql.*;

import static spark.Spark.stop;

public class AccountServiceMapImpl implements AccountService{
    private Account account;
    private Connection connection;

    public AccountServiceMapImpl() {

        String connectionUrl = "jdbc:sqlserver://localhost:1433;databaseName=master;user=sa;password=Test00Test";

        try {
            System.out.println("Connecting to SQL Server... ");
            connection = DriverManager.getConnection(connectionUrl);
            System.out.println("Done.");
            System.out.println("Checking for DB....");
            String sql = "USE BankDatabase;";
            try (Statement statement = connection.createStatement()) {
                statement.executeUpdate(sql);
                System.out.println("Done");
            } catch (Exception e1) {
                System.out.println("ERROR: DB is not exist");
                e1.printStackTrace();
                System.out.println("Create database");
                sql = "CREATE DATABASE [BankDatabase]";
                try (Statement statement = connection.createStatement()) {
                    statement.executeUpdate(sql);
                    System.out.println("Done.");
                } catch (Exception e2) {
                    System.out.println("FATAL ERROR: CAN NOT CREATE DB");
                    e2.printStackTrace();
                    connection.close();
                    stop();
                    return;
                }
            }
            System.out.println("Checking for table....");
            sql = new StringBuilder().append("IF NOT EXISTS(SELECT 1 FROM sys.Tables WHERE  Name = 'Accounts' AND Type = 'U') BEGIN").append(" CREATE TABLE Accounts ( ")
                    .append(" Id INT IDENTITY(1,1) NOT NULL PRIMARY KEY, ").append(" Name NVARCHAR(50) NOT NULL, ")
                    .append(" Balance INT NOT NULL, ").append(" Currency NVARCHAR(3) NOT NULL, ").append(" TimeOfTheLastOperation DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ").append(");")
                    .append("INSERT INTO dbo.Accounts (Name, Balance, Currency, TimeOfTheLastOperation) VALUES ")
                    .append("('8888 9999 00000 1111', 100, 'USD', CURRENT_TIMESTAMP), ")
                    .append("('8888 9999 00000 2222', 50, 'USD', CURRENT_TIMESTAMP), ")
                    .append("('8888 9999 00000 3333', 3000, 'MDL', CURRENT_TIMESTAMP);")
                    //.append("CREATE PROCEDURE GetAccountById @ById INT AS SELECT Id, Name, Balance, Currency, TimeOfTheLastOperation FROM dbo.Accounts WHERE Id = @ById;")
                    .append("END;")
                    .toString();
            //sql = "drop table Accounts; drop procedure GetAccountById;";
            try (Statement statement = connection.createStatement()) {
                statement.executeUpdate(sql);
                System.out.println("Done.");
            } catch (Exception e) {
                System.out.println("FATAL ERROR: CAN NOT CREATE TABLE");
                e.printStackTrace();
                connection.close();
                stop();
                return;
            }
        } catch (Exception e) {
            System.out.println("FATAL ERROR");
            e.printStackTrace();
            stop();
            return;
        }
        System.out.println("All done.");
    }

    @Override
    public Account getAccount(String id) {
        System.out.println("Read data for account...");
        String sql = new StringBuilder().append("GetAccountById @ById = ").append(id).append(";").toString();
        try(Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(sql)) {
            resultSet.next();
            account = new Account(resultSet.getString(1), resultSet.getString(2), resultSet.getInt(3), resultSet.getString(4), resultSet.getString(5));
            do{
                System.out.println(resultSet.getString(1) + " " + resultSet.getString(2) + " " + resultSet.getInt(3) + " " + resultSet.getString(4) + " " + resultSet.getString(5));
            } while(resultSet.next());
            System.out.println("Done.");
        } catch (Exception e) {
            System.out.println("ERROR: CAN NOT READ ACCOUNT");
            e.printStackTrace();
            return null;
        }
        return account;
    }

    @Override
    public boolean AccountExist(String id) {
        boolean flag = false;
        System.out.println("Searching for account...");
        String sql = new StringBuilder().append("SELECT count(Id) as flag FROM Accounts WHERE Id = ").append(id).append(";").toString();
        try(Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql)) {
            resultSet.next();
            System.out.println(resultSet.getInt(1));
            flag = resultSet.getInt(1) > 0 ? true : false;
            System.out.println("Done.");
        } catch (Exception e) {
            System.out.println("ERROR: CAN NOT FIND ACCOUNT");
            e.printStackTrace();
            return false;
        }
        return flag;
    }
}